package com.dotin.transactions.logging;

import com.dotin.transactions.xmlutility.XmlParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Log {

    public static void createLog(String logContext) {
        XmlParser xmlParser = new XmlParser();
        xmlParser.readFile("src/main/resources/terminal.xml");
        String path = xmlParser.getOutLogPath();
        try {
            FileWriter fileWriter = new FileWriter(path, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            String temp = logContext;
            bufferedWriter.write(temp);
            bufferedWriter.newLine();
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
