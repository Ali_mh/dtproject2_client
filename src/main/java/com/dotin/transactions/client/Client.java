package com.dotin.transactions.client;

import com.dotin.transactions.logging.Log;
import com.dotin.transactions.transaction.Response;
import com.dotin.transactions.transaction.Transaction;
import com.dotin.transactions.xmlutility.XmlParser;
import com.dotin.transactions.xmlutility.XmlWriter;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Client{
    Socket socket;
    int port;
    String serverAddress;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:SSS");
    InputStream inputStream;
    OutputStream outputStream;

    DataInputStream dataInputStream;
    DataOutputStream dataOutputStream;

    public Client() {
        try {
            List<Response> responses = new ArrayList<Response>();
            XmlParser xmlParser = new XmlParser();
            List<Transaction> transactionList = xmlParser.readFile("src/main/resources/terminal.xml");
            serverAddress = xmlParser.getServerIp();
            port = xmlParser.getPort();
            socket = new Socket(serverAddress, port);

            Log.createLog(dateFormat.format(new Date()) + " connected to server ... ServerIp=" + serverAddress
                    + " Server Port=" + port);
            System.out.println("Connected to server...");
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            dataInputStream = new DataInputStream(inputStream);
            dataOutputStream = new DataOutputStream(outputStream);

            for (int i = 0; i < transactionList.size(); i++) {
                String terminalInfo = "Terminal Id=" + xmlParser.getTerminalId() + " Terminal Type= " + xmlParser.getTerminalType();
                dataOutputStream.writeUTF(terminalInfo);
                objectOutputStream.writeObject(transactionList.get(i));

                Response response = (Response) objectInputStream.readObject();
                String log = dateFormat.format(new Date()) + " Transaction Result = " + response.getTransactionResult() +" "
                        + transactionList.get(i).toString() +"InitialBalance="+response.getInitialBalance()+ " DepositBalance = " + response.getDepositBalance();
                Log.createLog(log);
                System.out.println(log);
                responses.add(response);



                if (i < transactionList.size())
                    dataOutputStream.writeBoolean(true);
                else dataOutputStream.writeBoolean(false);

            }
            XmlWriter.createXml(responses);
            try {
                dataInputStream.close();
                dataOutputStream.close();
                objectOutputStream.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                Log.createLog(dateFormat.format(new Date()) + " " + e.getMessage());
            }
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
            Log.createLog(dateFormat.format(new Date()) + " " + e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            Log.createLog(dateFormat.format(new Date()) + " " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
            Log.createLog(dateFormat.format(new Date()) + " " + e.getMessage());
        }

    }


}
