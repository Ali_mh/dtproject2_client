package com.dotin.transactions.errorhandling;

import com.dotin.transactions.transaction.*;


public class ErrorManager {
    public static boolean inputError(Transaction transaction) {
        Integer transactionId = transaction.getTransactionId();
        Long transactionAmount = transaction.getTransactionAmount();

        try {
            if (transactionAmount<=0)
                throw new Exception("TransAmount cannot be negative or zero! Transaction Id=" + transactionId);
            return false;
        } catch (Throwable throwable) {
            System.out.println(throwable.getMessage());
            return true;
        }
    }
}
