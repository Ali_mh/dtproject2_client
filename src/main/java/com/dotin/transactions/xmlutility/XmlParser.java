package com.dotin.transactions.xmlutility;

import com.dotin.transactions.errorhandling.ErrorManager;
import com.dotin.transactions.transaction.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlParser {
    private String outLogPath;
    private Integer terminalId;
    private String terminalType;
    private String serverIp;
    private Integer port;

    public List<Transaction> readFile(String filePath) {
        List<Transaction> transactionList = new ArrayList<Transaction>();
        try {

            File terminal = new File(filePath);
            DocumentBuilderFactory dBFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dBFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(terminal);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("terminal");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    terminalId = Integer.valueOf(String.valueOf(element.getAttribute("id")));
                    terminalType = String.valueOf(element.getAttribute("type"));
                    serverIp = String.valueOf(element.getElementsByTagName("server").item(0).getAttributes().getNamedItem("ip").getNodeValue());
                    port = Integer.valueOf(element.getElementsByTagName("server").item(0).getAttributes().getNamedItem("port").getNodeValue());
                    outLogPath = String.valueOf(element.getElementsByTagName("outLog").item(0).getAttributes().getNamedItem("path").getNodeValue());
                }
            }

            NodeList nList = doc.getElementsByTagName("transaction");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Transaction transaction = new Transaction();
                    Integer transactionId;
                    Long transactionAmount;
                    Integer transactionDeposit;
                    String stringTransactionId = String.valueOf(element.getAttribute("id"));

                    try {
                        transactionId = Integer.parseInt(String.valueOf(element.getAttribute("id")));
                        transactionAmount = Long.valueOf(String.valueOf(element.getAttribute("amount")));
                        transactionDeposit = Integer.valueOf(String.valueOf(element.getAttribute("deposit")));
                    } catch (Exception e) {
                        System.out.println("Invalid Input! Transaction Id= " + stringTransactionId);
                        continue;
                    }
                    try {
                        transaction.setTransactionType(TransactionType.valueOf(String.valueOf(element.getAttribute("type"))));
                    } catch (Exception e) {
                        System.out.println("Invalid Transaction Type! Transaction Id= " + stringTransactionId);
                        continue;
                    }

                    transaction.setTransactionId(transactionId);
                    transaction.setTransactionAmount(transactionAmount);
                    transaction.setTransactionDeposit(transactionDeposit);
                    if (ErrorManager.inputError(transaction))
                        continue;

                    transactionList.add(transaction);

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return transactionList;
    }

    public String getOutLogPath() {
        return outLogPath;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public String getServerIp() {
        return serverIp;
    }

    public Integer getPort() {
        return port;
    }

    public String getTerminalType() {
        return terminalType;
    }
}
