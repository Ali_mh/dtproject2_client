package com.dotin.transactions.xmlutility;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.dotin.transactions.transaction.Response;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;


public class XmlWriter {

    public static void createXml(List<Response> responseList) {


        try {


            DocumentBuilderFactory documentBFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element responses = document.createElement("responses");
            document.appendChild(responses);
            for (Response responseObj : responseList) {
                String stringId = String.valueOf(responseObj.getTransactionId());
                String result = String.valueOf(responseObj.getTransactionResult());
                String initialBalance = String.valueOf(responseObj.getInitialBalance());
                String depositBalance = String.valueOf(responseObj.getDepositBalance());
                String transactionAmount =String.valueOf(responseObj.getTransactionAmount());
                String transactionType =String.valueOf(responseObj.getTransactionType());

                Element response = document.createElement("response");
                responses.appendChild(response);

                Attr idAttrib = document.createAttribute("id");
                idAttrib.setValue(stringId);
                response.setAttributeNode(idAttrib);

                Attr resultAttrib = document.createAttribute("result");
                resultAttrib.setValue(result);
                response.setAttributeNode(resultAttrib);

                Attr initialBAttrib = document.createAttribute("initialBalance");
                initialBAttrib.setValue(initialBalance);
                response.setAttributeNode(initialBAttrib);

                Attr depositBAttrib = document.createAttribute("depositBalance");
                depositBAttrib.setValue(depositBalance);
                response.setAttributeNode(depositBAttrib);

                Attr transactionAmountAttrib = document.createAttribute("transactionAmount");
                transactionAmountAttrib.setValue(transactionAmount);
                response.setAttributeNode(transactionAmountAttrib);

                Attr transactionTypeAttrib = document.createAttribute("transactionType");
                transactionTypeAttrib.setValue(transactionType);
                response.setAttributeNode(transactionTypeAttrib);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File("Response.xml"));
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            transformer.transform(domSource, streamResult);

        } catch (TransformerException e) {
            System.out.println(e.getMessage());
        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());

        }
    }


}
