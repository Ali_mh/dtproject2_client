package com.dotin.transactions.transaction;

import java.io.Serializable;
import java.math.BigDecimal;

public class Response implements Serializable {
    TransactionType transactionType;
    TransactionResult transactionResult;
    BigDecimal initialBalance;
    BigDecimal DepositBalance;
    Integer transactionId;
    Long transactionAmount;

    public Response(TransactionType transactionType, TransactionResult transactionResult, BigDecimal initialBalance, BigDecimal depositBalance, Integer transactionId, Long transactionAmount) {
        this.transactionType = transactionType;
        this.transactionResult = transactionResult;
        this.initialBalance = initialBalance;
        DepositBalance = depositBalance;
        this.transactionId = transactionId;
        this.transactionAmount = transactionAmount;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public BigDecimal getDepositBalance() {
        return DepositBalance;
    }

    public Integer getTransactionId() {
        return transactionId;
    }
}
