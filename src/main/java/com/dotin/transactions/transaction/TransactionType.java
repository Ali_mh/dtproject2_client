package com.dotin.transactions.transaction;


public enum TransactionType {
    deposit(0),
    withdraw(1);

    final int index;

    TransactionType(int index) {
        this.index = index;
    }

    public int getTransactionTypeIndex() {
        return index;
    }

}
